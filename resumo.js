
//Comentários: abcdefghijklmnopqrstuvwxyz

/* cometários com // (só a linha), ou /* */ /*o bloco
*/

/* variáveis:
 var nome = "Pedro Lucas"
 let idade = 21
 const pi = 3.14
 console.log(nome.length)

  * Operadores Aritméticos e Relacionais:
 - ++ incremento e -- decremento;
 - * multiplicação e / divisão;
 - ** potência e % resto;
 - == igual e === estritamente igual
   (1 == "1") True e (1 === "1") False;
 - != diferente e !== estritamente diferente  
   (1 == "1") False e (1 === "1") True;
 - > maior que e < menor que;  
 - >= maior ou igual e <= menor ou igual;
  
  * Operadores Lógicos:
 - && E;
 - || OU;
 - ! NOT; 

 */

 // print em pop-up
 var nome = prompt("Qual o seu nome?")
 var idade = prompt("Olá " + nome + ", qual a sua idade?")
 console.log("Sua idade é: " + idade + " anos.")
 // var idade é uma string, para converter para int/float, use parseInt/parseFloat
 console.log("Daqui a 2 anos você terá: " + (idade + 2) + " anos.")
 console.log("Daqui a 2 anos você terá: " + (parseInt(idade) + 2) + " anos.")
//alternativa
 idade = parseInt(idade)
 console.log("Daqui a 2 anos você terá: " + (idade + 2) + " anos.")

 /* Condicional if e else:
  if (condição) {

  }
  else {
  
  }

  * mais de 1 if:

  if (condição) {

  }
  else if (condição) {

  }
  else {

  }

  */

  var nome = prompt("Qual o seu nome?")
  var idade = parseInt(prompt("Qual a sua idade?"))
  
  if (idade >= 18 && idade <= 25){
      console.log(nome + " é maior de idade. E é jovem.")
  }
  else {
      if (idade > 25){
          console.log(nome + " é maior de idade.")
      }
      else {
          console.log(nome + " é menor de idade.")
      }
  }

/* Repetição com for:
  for (var contador = 0; contador <= 10; contador++){
    console.log(contador)
  }

  ou:
  var contador = 0
  for ( ; contador <= 10; ) {
    console.log("contador")
    contador++
  }

*/

/* Repetição while:
 while (contador < 10) {
  console.log(contador)
  contador++
 }
 
   DO while:
 do {
  contador++
  console.log(contador)
 } while(contador <10)

 while verifica condição antes de executar, e DO while executa antes de verificar

 break - quebra o laço
 continue - avança pra próxima iteração

*/

/* Arrays:
 - var lista = [1, 2, 3, 4]
 - var nomes = ["Pedro", "Rafa", "João", "Lucas"]
 - lista.pop() - remove o último;
 - lista.shift() - remove o primeiro e realoca os indexs;
 - lista.push() - adiciona no fim da lista;
 - lista.unshift() - aidciona no início da lista e realoca os indexs;
 - lista.lenght() - retorna o tamanho da lista;
 - lista.concat(lista2) - concatena listas;
  *** pode ser mais de 1: lista.concat(lista2, lista3, ...)
 - lista.splice(index, elemenos a deletar(pode ser 0), elemento a adicionar) - adiciona um elemento, ou mais, no index desejado. Remove quantos desejar a partir do index selexionado. Pode adicionar mais de 1 elemento. Pode ser usado apenas para remoção;
 - lista.slice(inicio,fim-1) - separa a parte selecionada da lista. Pra adição, remoção, a seu uso. Vai do inicio até o fim-1. Caso não especifique o fim, vai até o final da lista;
 - lista.sort() - ordena a lista em crescente ou alfabética;
 - lista.reverse() - inverte a lista;
 
*/

/* Array com laços:
 var nomes = ["Pedro", "Rafa", "João", "Lucas"]
 ## conteúdo/valor da lista
 for (nome of nomes){
  console.log(nome)
 }
 ## propriedade da lista (index)
 for (nome in nomes){
  console.log(nome)
 }

 */

 /* Funções com arrays:
  var idades = [10, 20, 30, 40]
  - idades.ForEach() -
    ***idades.ForEach((n) => {
         console.log(n)
       })
  - idades.map((n) => {
      return n* 2
    })
  - idades.filter((n) => {
      return n % 2 == 0
    })
  
*/

/* Funções:
  Exemplo 1:

  function maiorDeIdade(id){
      if (idade >= 18){
          return "É maior de idade."
      }
      else if (idade < 18){
          return "É menor de idade."
      }
      else {
          return "Por favor, insira um valor válido"
      }
  }

  let idade = 19

  console.log(maiorDeIdade(idade))

  Exemplo 2:

  function saudacoes(nome){
    alert('Olá ' + nome)
  }

  function inputName(callback){
      let nome = prompt("Por Favor digite seu nome")
      callback(nome)
  }

  inputName(saudacoes)
*/

/* Laços de Iteração:

  var Aluno = {
      nome: 'Pedro Lucas',
      idade: 21,
      matricula: '123456',
      matriculado: true,
      apresentacao: nome => "Olá, meu nome é " + nome + "!"
  }

 - for...in - Percorre um objeto retornando suas chaves, que podem ser usadas para acessar os valores;
  for (props in Aluno){
    console.log(Aluno) chave ou console.log(Aluno[props]) valor
  }

 - objeto.keys(objeto) - Retorna um array com as chaves do objeto;

 - objeto.values(objeto): Retorna um array com os valores do objeto.

*/


/* JSON
 var Aluno = {
       nome: 'Pedro Lucas',
       idade: 21,
       matricula: '123456',
       matriculado: true,
       apresentacao: nome => "Olá, meu nome é " + nome + "!"
 }
 var json = JSON.stringify(Aluno)
 var objeto = JSON.parse(json)

*/